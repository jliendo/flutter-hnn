import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hackernews/api_mgmt.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'api_mgmt.dart';

void main() {
  runApp(HNNApp());
}

class HNNApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hacker News Network',
      home: ChangeNotifierProvider(
        create: (context) => ApiMgmt(),
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text('Hacker News Network'),
          ),
          body: InitialLoadingScreen(),
        ),
      ),
    );
  }
}

class InitialLoadingScreen extends StatefulWidget {
  @override
  _InitialLoadingScreenState createState() => _InitialLoadingScreenState();
}

class _InitialLoadingScreenState extends State<InitialLoadingScreen> {
  @override
  Widget build(BuildContext context) {
    var data = Provider.of<ApiMgmt>(context);
    Widget w = data.isLoading ? Spinner() : TopNewsScreen();
    return w;
  }
}

class Spinner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SpinKitSpinningCircle(color: Colors.lightBlue),
        SizedBox(
          height: 15,
        ),
        Text('Loading...'),
      ],
    );
  }
}

class TopNewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var data = Provider.of<ApiMgmt>(context);
    List<Text> items = [];

    // construimos el ListView
    Widget myListView = ListView.separated(
      separatorBuilder: (context, index) {
        return Divider(
          thickness: 1,
          color: Colors.grey,
        );
      },
      itemCount: data.listTop10.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(data.listTop10[index]),
        );
      },
    );

    return Column(
      children: [
        Expanded(
          child: myListView,
        ),
        RaisedButton(
          onPressed: () {
            data.fetchItems();
          },
          child: Text("Refresh"),
        )
      ],
    );
  }
}
