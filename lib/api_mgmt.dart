import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:flutter/foundation.dart';

// la cantidad de historias que vamos a traer de HNN
const kNumeroDeHistorias = 20;

// haciendo el mixin con ChangeNotifier lo que hacemos es implementar
// el patron de provider para manejo de estado
class ApiMgmt with ChangeNotifier {
  // en el constructor mandamos a llamar la actualizacion de la lista de
  // items que traemos de hacker news network
  ApiMgmt() {
    _getHNTopHistories();
  }

  // atributo:
  // lista que guarda los titulos de los ultimos 'x' articulos de HNN
  List<String> listTop10;

  // atributo:
  // mientras se esta actualizando la lista es true. esta bandera es
  // importante, porque desde aqui en el GUI decidimos si mostramos el
  // spinner o mostramos el ListView con los items obtenidos del API de HNN
  bool isLoading = false;

  // esta funcion regresa un futuro con la promesa que finalmente va a
  // entregar una lista de Strings (una por cada articulo)
  Future<List<String>> _getHNTopHistories() async {
    String url;
    List<String> lastTopItems;
    http.Response response;

    // bandera que indica que estamos en mitad de la actualizacion de la
    // lista de articulos (estamos haciendo la llamada al api de HNN)
    // la documentacion del API esta en: https://github.com/HackerNews/API
    isLoading = true;
    notifyListeners();

    // ...ve por las ultimas historias. este endpoint solamente entrega lista
    // de IDs de articulos, luego hay que ir por cada uno de ellos de manera
    // individual
    try {
      url = 'https://hacker-news.firebaseio.com/v0/topstories.json';
      response = await http.get(url);
      if (response.statusCode == 200) {
        var itemsIds = convert.jsonDecode(response.body);

        // ...solo quedate con las primeras 'x' historias
        if (itemsIds.length > kNumeroDeHistorias) {
          itemsIds = itemsIds.sublist(0, kNumeroDeHistorias);
        } else {
          itemsIds = itemsIds.sublist(0, itemsIds.length);
        }

        // ...por cada uno de los IDs, vamos por su informacion completa, al
        // final del dia solo nos quedamos con los titulos de las historias
        lastTopItems = [];
        for (int i = 0; i < itemsIds.length; i++) {
          url =
              'https://hacker-news.firebaseio.com/v0/item/${itemsIds[i]}.json';
          response = await http.get(url);
          if (response.statusCode == 200) {
            var item = convert.jsonDecode(response.body);
            lastTopItems.add(item['title']);
          }
        } // del for para cada uno de los items
        listTop10 = lastTopItems;
        notifyListeners();
      }
    } catch (e) {
      print("error: $e");
    }

    // ya no estamos cargando
    isLoading = false;

    // le avisamos a todos los widgets que estan utilizando propiedades de
    // esta clase que se reconstruyan
    notifyListeners();
  }

  void fetchItems() async {
    await _getHNTopHistories();
    notifyListeners();
  }
}
